# Documento de Arquitetura de Software

## Visão Geral

Este documento descreve a arquitetura de software para um projeto que utiliza o React no frontend, quatro microsserviços implementados com FastAPI e um banco de dados hospedado no Google Cloud. A arquitetura foi projetada para ser escalável, resiliente e de alto desempenho. A arquitetura do sistema é baseada em uma abordagem de microsserviços, onde diferentes componentes são separados em serviços independentes e interagem através de interfaces bem definidas. A seguir, apresenta-se um esboço da arquitetura em alto nível:

![Arquitetura](../assets/arquitetura.png)
<figcaption style="text-align: center !important">
    Fonte: Próprio autor
  </figcaption>
</figure>


## Referências

NOGUEIRA, D. L. Ferramentas automatizadas para apoio ao projeto estruturado: uma aplicação do diagrama de entidade-relacionamento. Tese (Doutorado) — Universidade Federal do Rio de Janeiro, Rio de Janeiro, RS, 1988. Tese (Doutorado em Ciências em Engenharia de Sistemas e Computação). 

FRANCK, K. M.; PEREIRA, R. F.; FILHO, J. V. D. Diagrama entidade-relacionamento: uma ferramenta para modelagem de dados conceituais em engenharia de software. Research, Society and Development, v. 10, n. 8, 2021.

## Histórico de versão

|  Data  |   Versão   | Descrição |Autor(es)
|:------:|-----------:|:-------:|:---:|
| 08/04/2024 | 1.0 | Criando primeira versão | [Lucas Lima](https://gitlab.com/mibasFerraz)|
