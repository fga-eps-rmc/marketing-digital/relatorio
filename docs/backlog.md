# Backlog do Produto

## **1. Introdução**
Neste documento de Baxklog do Produto está organizado todas as funcionalidades do sequenciador elaborado no Lean Inception, os requisitos funcionais traduzidos em histórias de usuário e os requisitos não funcionais relacionadas ao desenvolvimento do FGAad.

## **2. Metodologia**
Utilizando as práticas do Lean Inception, a equipe mapeou as funcionalidades do projeto nas atividades de Brainstorm e na identificação das personas, conhecendo seus perfis e jornadas. Essas reuniões nos deram uma visão das necessidades do FGAad. Em seguida, nos reunimos em duplas para traduzir essas funcionalidades em User Stories do backlog do produto, garantindo que cada aspecto fosse alinhado com nossa visão do projeto.

## **3. Objetivo**
O objetivo deste documento é identificar e agrupar de forma adequada as necessidades do *FGAad*. Aqui estão apresentadas as User Stories, que surgiram a partir das funcionalidades identificadas durante as atividades de Lean Inception realizadas pela equipe de Marketing Digital.

## **3. Sequenciador**

### **1ª Onda**
| ID | Valor | Funcionalidade |
| -- | -- | -- |
| F01 | 🚴‍♂️🚴‍♂️ 💰💰💰 ❤️  | Ter controle de acesso |
| F02 | 🚴‍♂️🚴‍♂️ 💰💰💰 ❤️ ❤️ ❤️  | Cadastro das soluções tecnologicas |
| F03 | 🚴‍♂️ 💰💰 ❤️ ❤️ ❤️  | Rotular os projetos |

### **2ª Onda**
| ID | Valor | Funcionalidade |
| -- | -- | -- |
| F04 | 🚴‍♂️🚴‍♂️🚴‍♂️ 💰💰💰 ❤️❤️❤️ | Mantenedor atualiza projeto com noticias/atualizações |
| F05 | 🚴‍♂️🚴 💰💰 ❤️❤️ | Atualização das informações do projetos |
| F06 | 🚴‍♂️ 💰💰 ❤️❤️❤️ | Filtragem de projetos com base nas categorias |

### **3ª Onda**
| ID | Valor | Funcionalidade |
| -- | -- | -- |
| F07 | 🚴‍♂️🚴‍♂️ 💰💰💰 ❤️ | Captar informações com potencial analítico do projeto e usuário |
| F08 | 🚴‍♂️ 💰💰 ❤️❤️ | Exibição dos detalhes dos projetos na pesquisa do usuário |

É esperado o MVP fique pronto ao final desta onda.

### **4ª Onda**
| ID | Valor | Funcionalidade |
| -- | -- | -- |
| F08 | 🚴‍♂️🚴‍♂️🚴‍♂️ 💰💰💰 ❤️❤️❤️ | Dashboard do projeto para os mantenedores |
| F09 | 🚴‍♂️🚴‍♂️🚴‍♂️ 💰💰💰 ❤️❤️❤️ | Mais acessados da semana |

### **5ª Onda**
| ID | Valor | Funcionalidade |
| -- | -- | -- |
| F10 | 🚴‍♂️🚴‍♂️ 💰💰 ❤️❤️❤️ | Cadastro de newsletter |
| F11 | 🚴‍♂️🚴‍♂️ 💰💰💰 ❤️❤️ | Níveis de acesso para diferentes tipos de usuário |
| F12 | 🚴‍♂️ 💰💰 ❤️❤️❤️ | Filtrar assuntos que deseja receber na newsletter |

## **4. Requisitos Funcionais**
As User Stories deste documento seguem o padrão papel-ação-valor:

>*Eu como __tipo de usuário__,
desejo __uma ação__
para que __um benefício/valor__.*

| ID | User Stories | Tasks | Prioridade | 
| -- | -- | -- | -- |
| US01 | Como um mantenedor de projeto, eu quero ter a capacidade de cadastrar, editar e gerenciar projetos na plataforma, para divulgar os serviços por ele prestados e encontrar patrocinadores. | <ul> <li> Implementar autenticação; <li> Definir niveis de acesso; <li> Definir e implementar formulário de  cadastro; </ul> | Alta |
| US02 | Como um mantenedor de projeto, eu quero poder cadastrar imagens e outras mídias do meu projeto, para melhor divulgar as informações necessárias | <ul> <li> Definir quais tipos de mídia são permitidos <li> Criar página para editar projetos </ul> | Alta | 
| US03 | Como um mantenedor de projeto, eu quero ter a capacidade de rotular os projetos com labels, para organizar e categorizar os projetos com o objetivo de identificar facilmente projetos similares. | <ul> <li> Definir categorias; <li> Adicionar ao formulário de cadastro;</ul> | Alta |
| US04 | Como um mantenedor de projeto, eu quero compartilhar atualizações e notícias do projeto, para manter os usuários interessados atualizados nas novidades do projeto.  | <ul> <li>Fazer migration para tabela de notícia/atualização</li> <li> Criar crud para notícia/atualização; <li> Adicionar página para cadastro de Notícias/Atualizações;</ul> | Alta |
| US05 | Como um usuário, quero poder filtrar os projetos com base nas categorias, para que eu possa encontrar rapidamente os projetos que são do meu interesse.  | <ul><li>Adicionar lógica de filtragem ao Backend</li> <li>Adicionar Opções de Filtragem na Interface</li> <li>Atualizar página de visualização de projetos</li> <li>Testar a funcionalidade de filtragem</li></ul> | Alta |
| US06 | Como mantenedor de projeto, eu quero ter acesso às informações de tráfego da plataforma, como quantidade de acessos, tempo de leitura de página, quantidade de cliques e etc. | <ul><li>Definir quais dados de tráfego coletar; <li>Definir ferramenta de coleta dos dados; <li>Estruturar dados obtidos;</ul> | Alta |
| US06 | Como usuário, quero conseguir ver algumas informações básicas do projeto enquanto estiver na página de busca, sem a necessidade de abrir uma nova página para isso | <ul> <li> Definir quais informações devem ser exibidas; <li> Criar modal; | Alta |
| US07 | Como mantenedor do projeto, eu quero que as informações relacionadas com o tráfego seja exibidas em uma Dashboard. | <ul> <li> Criar página de Dashboard; <li> Criar gráficos e tabelas interativos; | Média |
| US08 | Como usuário, quero saber quais foram os projetos mais acessados da semana | <ul> <li> Criar página de mais acessados da semana; | Média |
| US09 | Como usuário, eu quero me cadastrar em newsletters dos projetos e temas de meu interesse | <ul> <li> Permitir favoritar projetos e temas; | Baixa |
| US10| Como mantenedor do projeto, eu quero enviar para a lista de usuários interessados atualizações e notícias sobre o meu projeto | <ul> <li> Envio de newsletter para lista de usuários; | Baixa |


## **5. Requisitos Não Funcionais**

|  ID   | Requisito |
| :--:  | :--: |
| RNF01 | Criar banco de dados |
| RNF02 | Validar entrada de dados |

### **Bibliografia**
- Beck, Kent. Programação extrema (xp) explicada. bookman, 2004.
- Caroli, Paulo. "Lean inception." São Paulo, BR: Caroli. org (2017).

## **Histórico de Versão**

| Data | Versão | Descrição | Autor |
|--|--|--|--|
| 09/04/2024 | 0.1 | Estrutura inicial do documento | Douglas Farias e Edvan Gomes |
| 09/04/2024 | 0.2 | Adicionado a metodologia utilizada, o objetivo e algumas user stories da primeira onda do sequenciador | Douglas Farias |
| 10/04/2024 | 0.3 | Adicionado as ondas do projeto | Douglas Farias |
| 10/04/2024 | 0.4 | Ajustando primeiras USs e criando primeiros RNF | Edvan Gomes |
| 16/04/2024 | 0.5 | Adicionado introdução e USs da 2ª onda | Douglas Farias |
| 16/04/2024 | 0.6 | Adicionando demais ondas | Edvan Gomes |
| 16/04/2024 | 0.7 | Adicionando prioridade em cada US | Edvan Gomes |
