# Estrutura Analítica do Projeto - EAP

## Versionamento
| Versão | Data | Modificação | Autor |
|--|--|--|--|
|1.0| 12/04/2024 | Criação do documento | Pedro Nogueira e Iago Cabral |


## O que é a EAP?
A EAP (Estrutura Analítica do Projeto), do inglês Work Breakdown Structure (WBS), é uma subdivisão hierárquica do trabalho do projeto em partes menores, mais facilmente gerenciáveis. Seu objetivo primário é organizar o que deve ser feito para produzir as entregas do projeto.



## Para o que serve?
A EAP garante ao gerente de projetos a visibilidade das principais entregas, facilitando o controle de tempo e de custo. A EAP serve para ajudar na organização e no planejamento de projetos, facilitando o entendimento do escopo do trabalho, a alocação de recursos, o acompanhamento do progresso e o controle de custos. Ela proporciona uma visão hierárquica e estruturada do projeto, dividindo-o em componentes menores e mais gerenciáveis, o que facilita a atribuição de responsabilidades e a identificação de dependências entre as diferentes partes do projeto.

## EAP - Projeto DNIT

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="725" height="350" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FwyP4rCbTAp4ytmqZZlrHl4%2FUntitled%3Ftype%3Dwhiteboard%26node-id%3D0%253A1%26t%3DAsXQSLlu8Tqc0uNp-1" allowfullscreen></iframe>

## Referências

> [1]EAP (Estrutura Analítica do Projeto): o que é, como fazer e qual a diferença entre EAP e Cronograma. 2018. Disponível em:  https://www.euax.com.br/2018/12/eap-estrutura-analitica-projeto/. Acesso em: 12 de Abril de  2024.