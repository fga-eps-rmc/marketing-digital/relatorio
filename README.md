# Relatorio Marketing Digital

## Semana 1-2 (25/03-05/04)

### Atividades Desenvolvidas

Canvas MVP - https://www.figma.com/file/cJRWyS2X4GFIXxXGkSZwD9/GP4--MarketingDigital?type=whiteboard&node-id=0%3A1&t=hquNXiEkOsl5eHjz-1

<!-- Todos da equipe respondem -->

| Matricula | Nome | Gitlab | Atividade Desenvolvida | Desafios | Soluções |
| -- | -- | -- | -- | -- | -- |
| 17/0140571 | Douglas Farias de Castro | douglasffcastro | Atividades da Lean Inception para elaboraçãdo do Canva MVP | Definição do Escopo e Tempo para Elaboração | Reuniões Diárias e prorrogação do prazo para sexta |
| 17/0032591 | Edvan                    | edvan.bgjunior | LeanInception e facilitação de todos rituais presenciais | Tempo e definições abertas | Programação do Cronograma com duração e assuntos bem definidos |
| 19/0088745 | Iago Cabral  | iagocabral | LeanInception | Escopo e Tempo | reuniões, Cronograma e Escopo do Mvp |
| 18/0105345 | Lucas Lima Ferraz | mibasFerraz | Criação do repositório da disciplina e sua organização. LeanInception | Adequar uma disciplina nova ao Gitlab. Escopo e tempo | Conversar com o professor Ricardo Chaim e ajustar as divergências. Dailys, seguir as recomendações do Lean inception |
| 19/0094486 | Pedro Nogueira | phnog | LeanInception | Escopo e tempo | Reuniões diárias e desenvolvimento do MVP |
| 17/0020525 | Pedro Lima | pedrolimass | LeanInception | Escopo e tempo | Reuniões, Desenvolvimento do Cronograma juntamente com o MVP |
| 16/0141842 | Philipe Rosa Serafim | philipeserafim | LeanInception | Tempo e definições abertas | Programação do Cronograma com duração e assuntos bem definidos |

### Intercorrências

<!-- Área reservada ao líder técnico -->

| Intercorrência | Intervenção |
| -- | -- |
| Dificuldade em encontrar horários em comum | Criar um documento de disponibilidade do grupo e pegar os horários com mais participantes livres |
